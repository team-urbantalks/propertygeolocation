from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect, HttpResponseForbidden
from django.template import loader
import json
from django.conf import settings
#import shapely
#from shapely.geometry import Point, Polygon, shape
#import sqlalchemy
#from sqlalchemy import create_engine
import psycopg2
from psycopg2 import pool
import psycopg2.extras
import threading
import sqlalchemy
from sqlalchemy import create_engine 

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.views import LoginView


# Postgres Connection Pool
def initdb(db_config):
    try:
        geolocdb_pool = psycopg2.pool.ThreadedConnectionPool(5, 20, host=db_config['host'], database=db_config['database'], user=db_config['user'], password=db_config['password'], cursor_factory=psycopg2.extras.RealDictCursor)#, connect_timeout=3)
        print("PostgreSQL Connection pool created with Database: ", db_config['database'], " at host ", db_config['host'], "successfully")
        return geolocdb_pool
    except (Exception, psycopg2.DatabaseError) as error :
        print ("Error while connecting to PostgreSQL database: ", db_config, " | Error:", error, "   |   ", psycopg2.DatabaseError)

geolocdb_pool = initdb(settings.GEOLOCATION_DB)


def pingdb(pool, dbconfig):
	threading.Timer(60, pingdb, args=[pool, dbconfig]).start()
	try:
		if pool:
			conn = pool.getconn()
			cur = conn.cursor()
			query = "select 1 as test"
			cur.execute(query)
			data = cur.fetchone()
			print("Sucessfully pinged PostgreSQL Connection pool with Database", dbconfig['database'], "| Query : ", query, " | Data: ", data)
			cur.close()
			pool.putconn(conn)
	except Exception as error :
		print("PostgreSQL Connection pool ping failed reinitializing Database:", dbconfig, " | Error: ", error)
		pool = initdb(dbconfig)

#pingdb(geolocdb_pool, settings.GEOLOCATION_DB)

def getPoolTested(db_pool=geolocdb_pool, dbconfig=settings.GEOLOCATION_DB):
	try:
		conn = db_pool.getconn()
		cur = conn.cursor()
		query = "select 1 as test"
		cur.execute(query)
		data = cur.fetchone()
		cur.close()
		db_pool.putconn(conn)
		return db_pool
	except Exception as error :
		print("PostgreSQL Connection pool ping failed reinitializing Database:", dbconfig, " | Error: ", error)
		db_pool = initdb(dbconfig)
		return db_pool

def querydb(query, db_pool=geolocdb_pool, dbconfig=settings.GEOLOCATION_DB):
	try:
		#conn = db_pool.getconn()
		conn = getPoolTested(db_pool, dbconfig).getconn()
		cur = conn.cursor()
		cur.execute(query)
		data = cur.fetchall()
		cur.close()
		db_pool.putconn(conn)
		return data
	except Exception as e:
		rollback_db()
		raise Exception(e)

def execdb(query, db_pool=geolocdb_pool, dbconfig=settings.GEOLOCATION_DB):
	try:
		#conn = db_pool.getconn()
		conn = getPoolTested(db_pool, dbconfig).getconn()
		cur = conn.cursor()
		cur.execute(query)
		conn.commit()		
		data = cur.fetchall()
		cur.close()
		db_pool.putconn(conn)
		return data
	except Exception as e:
		rollback_db()
		raise Exception(e)

 # Use rollback_db if you get any transaction related error
def rollback_db(db_pool=geolocdb_pool, dbconfig=settings.GEOLOCATION_DB):
	#conn = db_pool.getconn()
	conn = getPoolTested(db_pool, dbconfig).getconn()
	cur = conn.cursor()
	cur.execute("ROLLBACK")
	conn.commit()
	cur.close()
	geolocdb_pool.putconn(conn)

def getconfig(request):
	appconfig = {
		'geoserverapi' : settings.GEOSERVER_API
	}
	returnhtml = 'appconfig = ' + json.dumps(appconfig)
	return HttpResponse(returnhtml)

# Create Postgres engines
def _pg_engine(dbconfig):
	engine = create_engine('postgresql://'+ dbconfig['user'] +':'+ dbconfig['password'] +'@'+ dbconfig['host'] +':'+ dbconfig['port'] +'/'+ dbconfig['database'])
	return engine


# Check for user permissions
def check_email_id(user):
	#print(dir(user))
	if user.email:
		email_domain = user.email.split('@')[-1].split('.')[0]
		user_is_allowed = user.groups.filter(name='allowed').exists()
		user_is_blocked = user.groups.filter(name='blocked').exists()
		if (email_domain.upper() == 'AVENUE' or user_is_allowed) and (~user_is_blocked) :
			return True
		else:
			#user.session_set.all().delete()
			return False
	else:
		#user.session_set.all().delete()
		return False

# Create your views here.

'''
def index(request):
    template = loader.get_template('base.html')
    context = {
        'latest_question_list': 'latest_question_list',
    }
    return HttpResponse(template.render(context, request))
'''


def app_login(request):
	return LoginView.as_view(template_name='login.html')(request)

@login_required
def propertyMap(request):	
	user_is_authorised = check_email_id(request.user)
	if user_is_authorised:
		template = loader.get_template('map.html')
		context = {
			'latest_question_list': 'latest_question_list',
		}
		return HttpResponse(template.render(context, request))
	else:
		template = loader.get_template('public-map.html')
		return HttpResponse(template.render(None, request))

@login_required
@user_passes_test(check_email_id)
def myProperties(request):
	template='my-properties.html'
	user_name = request.user.username
	query = "SELECT * FROM public.geolocated_property WHERE user_name = '" + user_name + "'" 
	properties = querydb(query)
	return render(request, template, {
		'my_properties' : properties
	})

@login_required
#@user_passes_test(check_email_id)
def allProperties(request):
	template='all-properties.html'
	query = "SELECT * FROM public.geolocated_property" 
	properties = querydb(query)
	return render(request, template, {
		'all_properties' : properties
	})

@login_required
@user_passes_test(check_email_id)
def addPropertyApi(request):
	#print(request)
	if request.method == 'POST': # If the form has been submitted...
		data = json.loads(request.body.decode("utf-8"))
		user_name = request.user.username
		user_email = request.user.email
		try:
			geometry = data['property']['features'][0]['geometry']
			fieldremarks = data['fieldremarks']
			query = "INSERT INTO public.geolocated_property(user_name, user_email, fieldremarks, geometry) VALUES('" + user_name + "', '" + user_email +  "', '"+ fieldremarks +"', ST_SetSRID(ST_GeomFromGeoJSON($$"+ str(geometry) +"$$), 4326))  RETURNING propid"
			data = execdb(query, db_pool=geolocdb_pool, dbconfig=settings.GEOLOCATION_DB )
			return HttpResponse(json.dumps(data))
		except:
			return HttpResponse('failed')
	else:
		return HttpResponse('Sandeep') # Redirect after POST
		
@login_required
def getPropertyApi(request):
	propid = request.GET.get('propid', False)
	if propid:
		propids = propid.split(',')
		valid_propids = [ ]
		invalid_propids = []
		for x in propids:
			try:
				valid_propids.append(str(int(x)))
			except:
				invalid_propids.append(x)
		returnable_data ={
			"invalid_propids" : invalid_propids,
			"valid_propids" : valid_propids
		}
		if len(valid_propids):
			valid_propids_str = ",".join(valid_propids)
			query = "SELECT * FROM public.geolocated_property WHERE propid IN (%s) " % (valid_propids_str)
			pd_query = '''
			SELECT jsonb_build_object(
				'type',     'FeatureCollection',
				'features', jsonb_agg(feature)
			) AS fc
			FROM ( 
				SELECT jsonb_build_object(
					'type',       'Feature',
					'id',         propid,
					'geometry',   ST_AsGeoJSON(geometry)::jsonb,
					'properties', to_jsonb(row) - 'geometry'
				) AS feature
				FROM (%s) row
			) feature;
			''' % (query)
			data = querydb(pd_query)
			data = json.loads(json.dumps(data))
			if len(data):
				prop_data = data[0]["fc"]
				if (prop_data["features"]):
					returnable_data['property_geojson'] = prop_data
				else:
					returnable_data["Error"] = "No Results for Supplied Property IDs"
			else:
				returnable_data["Error"] = "No Results for Supplied Property IDs"
		else:
			returnable_data["Error"] = "No Valid Property IDs Supplied"
	else:
		returnable_data = {
			"Error" : "No Property IDs Supplied"
		}

	return JsonResponse(returnable_data, safe = False)



