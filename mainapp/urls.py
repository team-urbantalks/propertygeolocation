from django.urls import path
from django.conf.urls import url, include

from . import views
from django.contrib.auth import views as authviews

urlpatterns = [
    url(r'^auth/', include('social_django.urls', namespace='social')),
    url(r'^accounts/login/', views.app_login, name='login'),
    url(r'^logout/$', authviews.LogoutView.as_view(), name='logout'),
    #url(r'^$', views.propertyMap, name='index'),
    path('', views.propertyMap, name='home'),
    path('propertyMap/', views.propertyMap, name='propertyMap'),
    path('my-properties/', views.myProperties, name='myProperties'),
    path('all-properties/', views.allProperties, name='allProperties'),
    path('search/', views.search_map, name='search_map'),
    
    
    # APIs
    path('api/v1/add-property/', views.addPropertyApi, name='addPropertyApi'),
    path('api/v1/get-property/', views.getPropertyApi, name='getPropertyApi'), 
    path('api/v1/delete-property/', views.delete_property_api, name='delete_property_api'), 

]