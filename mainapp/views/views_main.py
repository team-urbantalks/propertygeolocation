from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect, HttpResponseForbidden
from django.template import loader
import json
from django.conf import settings
#import shapely
#from shapely.geometry import Point, Polygon, shape
#import sqlalchemy
#from sqlalchemy import create_engine
import psycopg2
from psycopg2 import pool
import psycopg2.extras
import threading
import sqlalchemy
from sqlalchemy import create_engine 

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.views import LoginView


from common.databases import * 

# Check for user permissions
def check_email_id(user):
	#print(dir(user))
	if user.is_authenticated and user.email:
		email_domain = user.email.split('@')[-1].split('.')[0]
		user_is_allowed = user.groups.filter(name='allowed').exists()
		user_is_blocked = user.groups.filter(name='blocked').exists()
		if (email_domain.upper() == 'AVENUE' or user_is_allowed) and (~user_is_blocked) :
			return True
		else:
			#user.session_set.all().delete()
			return False
	else:
		#user.session_set.all().delete()
		return False

# Create your views here.

'''
def index(request):
    template = loader.get_template('base.html')
    context = {
        'latest_question_list': 'latest_question_list',
    }
    return HttpResponse(template.render(context, request))
'''


def app_login(request):
	return LoginView.as_view(template_name='login.html')(request)


@login_required
@user_passes_test(check_email_id)
def search_map(request):	
    template = loader.get_template('map-search.html')
    return HttpResponse(template.render(None, request))


#@login_required
def propertyMap(request):	
	user_is_authorised = check_email_id(request.user)
	if user_is_authorised:
		template = loader.get_template('map.html')
		context = {
			'latest_question_list': 'latest_question_list',
		}
		return HttpResponse(template.render(context, request))
	else:
		template = loader.get_template('public-map.html')
		return HttpResponse(template.render(None, request))

@login_required
@user_passes_test(check_email_id)
def myProperties(request):
	template='my-properties.html'
	user_name = request.user.username
	query = f"""
			SELECT * 
			FROM public.geolocated_property 
			WHERE 
				user_name = '{user_name}'
				AND
				active = 1 
			""" 
	properties = querydb(query, 'geolocation_db')
	return render(request, template, {
		'my_properties' : properties
	})

@login_required
#@user_passes_test(check_email_id)
def allProperties(request):
	template='all-properties.html'
	query = """
			SELECT * 
			FROM public.geolocated_property 
			WHERE active = 1
			""" 
	properties = querydb(query, 'geolocation_db')
	return render(request, template, {
		'all_properties' : properties
	})

@login_required
@user_passes_test(check_email_id)
def addPropertyApi(request):
	#print(request)
	if request.method == 'POST': # If the form has been submitted...
		data = json.loads(request.body.decode("utf-8"))
		user_name = request.user.username
		user_email = request.user.email
		try:
			geometry = data['property']['features'][0]['geometry']
			fieldremarks = data['fieldremarks']
			query = "INSERT INTO public.geolocated_property(user_name, user_email, fieldremarks, geometry) VALUES('" + user_name + "', '" + user_email +  "', '"+ fieldremarks +"', ST_SetSRID(ST_GeomFromGeoJSON($$"+ str(geometry) +"$$), 4326))  RETURNING propid"
			data = execdb(query, 'geolocation_db' )
			return HttpResponse(json.dumps(data))
		except Exception as e:
			return_message = f"Failed Adding Property| Error: {str(e)}"
			return HttpResponse(return_message)
	else:
		return HttpResponse('Sandeep') # Redirect after POST

@login_required
@user_passes_test(check_email_id)
def delete_property_api(request):
	#print(request)
	if request.method == 'POST': # If the form has been submitted...
		data = json.loads(request.body.decode("utf-8"))
		#user_name = request.user.username
		user_email = request.user.email
		try:
			query = f"""
					UPDATE public.geolocated_property
					SET
						active = 0
					WHERE
						propid = {data['propid']}
						AND
						user_email = '{user_email}'
					;
					"""
			execdb(query, 'geolocation_db')
			return HttpResponse("Successfully deleted Property")
		except Exception as e:
			return_message = f"Failed Deleting Property| Error: {str(e)}"
			return HttpResponse(return_message)
	else:
		return HttpResponse('Sandeep') # Redirect after POST

#@login_required
def getPropertyApi(request):
	propid = request.GET.get('propid', False)
	if propid:
		propids = propid.split(',')
		valid_propids = [ ]
		invalid_propids = []
		for x in propids:
			try:
				valid_propids.append(str(int(x)))
			except:
				invalid_propids.append(x)
		returnable_data ={
			"invalid_propids" : invalid_propids,
			"valid_propids" : valid_propids
		}
		if len(valid_propids):
			valid_propids_str = ",".join(valid_propids)
			query = "SELECT * FROM public.geolocated_property WHERE propid IN (%s) " % (valid_propids_str)
			pd_query = '''
			SELECT jsonb_build_object(
				'type',     'FeatureCollection',
				'features', jsonb_agg(feature)
			) AS fc
			FROM ( 
				SELECT jsonb_build_object(
					'type',       'Feature',
					'id',         propid,
					'geometry',   ST_AsGeoJSON(geometry)::jsonb,
					'properties', to_jsonb(row) - 'geometry'
				) AS feature
				FROM (%s) row
			) feature;
			''' % (query)
			data = querydb(pd_query, 'geolocation_db')
			data = json.loads(json.dumps(data))
			if len(data):
				prop_data = data[0]["fc"]
				if (prop_data["features"]):
					returnable_data['property_geojson'] = prop_data
				else:
					returnable_data["Error"] = "No Results for Supplied Property IDs"
			else:
				returnable_data["Error"] = "No Results for Supplied Property IDs"
		else:
			returnable_data["Error"] = "No Valid Property IDs Supplied"
	else:
		returnable_data = {
			"Error" : "No Property IDs Supplied"
		}

	return JsonResponse(returnable_data, safe = False)


