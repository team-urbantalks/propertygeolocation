//----------------- Context Menu Starts ---------------
function centerMap (e) {
    map.panTo(e.latlng);
  }
  
  function zoomIn (e) {
    map.zoomIn();
  }
  
  function zoomOut (e) {
    map.zoomOut();
  }
  //----------------- Context Menu Ends ---------------
  //------------ Leaflet Functions start -----------
  function leaflet_initmap(mapdiv){
    var map = L.map(mapdiv, {
      zoomControl: false,
      scrollWheelZoom: true,
      center: [16.5, 80.6],
      zoom: 7,
      maxZoom: 20,
      minZoom: 4,
      doubleClickZoom: false,
      //zoomDelta: 0.5,
      wheelPxPerZoomLevel: 100,
      contextmenu: true,
      contextmenuWidth: 160,
      contextmenuItems: [{
          text: 'Clear Search',
          icon: 'static/images/icons/load.png',
          callback: clear_search_results,
          disabled: false,
      }, '-', {
          text: 'Center map here',
          icon: 'static/images/icons/zoom-extent.png',
          disabled: false,
          callback: centerMap
      }, {
          text: 'Zoom in',
          icon: 'static/images/icons/zoom-in.png',
          callback: zoomIn
      }, {
          text: 'Zoom out',
          icon: 'static/images/icons/zoom-out.png',
          callback: zoomOut
      }]
    });
    L.control.scale({
        metric: true,
        imperial: false
    }).addTo(map);
    L.control.zoom({  //add zoom control to right side
      position:'bottomleft',
    }).addTo(map);
    /*L.control.navbar({
      position:'topleft',
    }).addTo(map);*/
    return map;
  }
  function leaflet_addbasemaps(map){
    var basemapminzoomlevel = 2
    var google_standard_roadmap_layer = L.tileLayer('https://www.google.co.in/maps/vt/lyrs=m&hl=en&x={x}&y={y}&z={z}&s=Ga', {
      detectRetina: true,
      maxNativeZoom: 24,
      minZoom: basemapminzoomlevel,
      maxZoom: 30,
    }).setZIndex(1);
    var google_satellite_layer = L.tileLayer('https://www.google.co.in/maps/vt/lyrs=s&hl=en&x={x}&y={y}&z={z}&s=Ga', {
      detectRetina: true,
      maxNativeZoom: 24,
      minZoom: basemapminzoomlevel,
      maxZoom: 30
    }).setZIndex(2);
    var google_hybrid_layer = L.tileLayer('https://www.google.co.in/maps/vt/lyrs=y&hl=en&x={x}&y={y}&z={z}&s=Ga', {
      detectRetina: true,
      maxNativeZoom: 24,
      minZoom: basemapminzoomlevel,
      maxZoom: 30
    }).setZIndex(3).addTo(map);
    var google_terrain_layer = L.tileLayer('https://www.google.co.in/maps/vt/lyrs=p&hl=en&x={x}&y={y}&z={z}&s=Ga', {
      detectRetina: true,
      maxNativeZoom: 24,
      minZoom: basemapminzoomlevel,
      maxZoom: 30
    }).setZIndex(4);
    var google_traffic_layer = L.tileLayer('https://www.google.co.in/maps/vt/lyrs=m@221097413,traffic&hl=en&x={x}&y={y}&z={z}&s=Ga', {
      detectRetina: true,
      maxNativeZoom: 24,
      minZoom: basemapminzoomlevel,
      maxZoom: 30
    }).setZIndex(5);
    var blankbasemap = L.tileLayer.wms('').setZIndex(10);
    var basemaps = {
      "Google Standard": google_standard_roadmap_layer,
      "Google Satellite": google_satellite_layer,
      "Google Hybrid": google_hybrid_layer,
      "Google Terrain": google_terrain_layer,
      "Google Transit": google_traffic_layer,
      "Remove Basemap": blankbasemap
    };
    drawLayerControl(basemaps, null);
    return basemaps;
  }
  
  var layercontroller;
  function drawLayerControl(basemaps, layers) {
    try {
      $('.leaflet-control-layers').remove();
      layercontroller.removeFrom(map);
    } catch (err) {
      console.log(err);
    }
    layercontroller = L.control.layers(
      basemaps,
      layers,
      {
        collapsed: true,
        autoZIndex: false,
        position: 'topright'
      }
    ).addTo(map);
  }
  
  function leaflet_removelayer(layer) {
    if (map.hasLayer(layer)) {
      map.removeLayer(layer);
    }
  }
  
  //--------- Draw Controls start -------------
  var drawmode = 0;
  function leaflet_add_drawing_tools(map){
    var style = {
        "color": "#FF2D00",
        "weight": 2,
        "opacity": 1,
        "fillOpacity": 0.3,
        "fillColor": "#f00",
        "selectedColor": "#000000",
    }
    drawnItems = new L.FeatureGroup();
    map.addLayer(drawnItems);
    var drawControl = new L.Control.Draw({
      draw: {
         polygon : {
           allowIntersection: false,
           showArea: true,
           metric: 'metric',
           shapeOptions: style,
           bubblingMouseEvents: false
         },polyline: {
           metric: 'metric',
           shapeOptions: style,
           bubblingMouseEvents: false
         },
         circle: false,
         circlemarker: false,
         marker: {
           bubblingMouseEvents: false
         },
         rectangle: false
       },
       edit: {
         featureGroup: drawnItems,
         poly : {
           allowIntersection : false
         }
       },
    });
    map.addControl(drawControl);
    map.on('draw:edited', function (e) {
        var layers = e.layers;
        layers.eachLayer(function (layer) {
            //do whatever you want; most likely save back to db
        });
    });
    // Truncate value based on number of decimals
    var _round = function(num, len) {
        return Math.round(num*(Math.pow(10, len)))/(Math.pow(10, len));
    };
    // Generate popup content based on layer type
    // - Returns HTML string, or null if unknown object
    var getPopupContent = function(layer) {
        // Marker - add lat/long
        if (layer instanceof L.Marker || layer instanceof L.CircleMarker) {
        var directionpoint = layer.getLatLng();
        let mark_prop_url = buildUrl( absolutePath('/'), {latlng: [directionpoint.lat, directionpoint.lng].toString() } );
        var popuphtml = `
            <label style="width: 100%; text-align: center;">Dropped Pin</label></br>
            <label>Lat Long:</label> <a title="copytoclipboard" href="javascript:copytoclipboard('`+ directionpoint.lat +' '+ directionpoint.lng +`')">`+ directionpoint.lat.toFixed(4) +` `+ directionpoint.lng.toFixed(4) +`&nbsp;<i class="fa fa-clone" aria-hidden="true"></i></a>
            </br>
            <a href="${mark_prop_url}">
                <div class="btn btn-primary">
                    Mark Property Here
                </div>
            </a>
            </br>
            </br>
        `;
          var directionhtml = `<div class="text-center" style="padding:10px; background: #337ab7;">
                            <span style="color:#fff;"><i class="fa fa-map-signs" aria-hidden="true"></i>&nbsp;Get Directions</span>
                            <div style="width:100%; margin-top:10px;">
                              <a target="_blank" href="http://maps.google.com/maps?saddr=my+location&amp;daddr=`+ directionpoint.lat +`,`+ directionpoint.lng +`">
                                <div style="border:2px solid rgba(0,0,0,0.2); font-size: 12px; height: 30px; padding: 4px 5px; color: #333 !important;" class="btn btn-light">
                                  Take me there
                                </div>
                              </a>
                              <a target="_blank" href="http://maps.google.com/maps?saddr='+ `+ directionpoint.lat +`,`+ directionpoint.lng +`&amp;daddr=">
                                <div style="border:2px solid rgba(0,0,0,0.2); font-size: 12px; height: 30px; padding: 4px 5px; color: #333 !important;" class="btn btn-light">
                                  Take me from
                                </div>
                              </a>
                            </div>
                           </div>`
          popuphtml = popuphtml + directionhtml;
          return popuphtml;
        // Circle - lat/long, radius
        } else if (layer instanceof L.Circle) {
            var center = layer.getLatLng(),
                radius = layer.getRadius();
            return "Center: "+strLatLng(center)+"<br />"
                  +"Radius: "+_round(radius, 2)+" m";
        // Rectangle/Polygon - area
        } else if (layer instanceof L.Polygon) {
            var latlngs = layer._defaultShape ? layer._defaultShape() : layer.getLatLngs(),
            area = L.GeometryUtil.geodesicArea(latlngs);
            area = formatunits(area, 'area', 1);
            var popuphtml = `<label style="width: 100%; text-align: center;">Drawn Area</label></br>
                            <div>`+ area +`</div>`;
            console.log(popuphtml)
            return popuphtml;
            //return "Area: "+L.GeometryUtil.readableArea(area, true);
        } else if (layer instanceof L.Polyline) {
            var latlngs = layer._defaultShape ? layer._defaultShape() : layer.getLatLngs(),
            distance = 0;
            if (latlngs.length < 2) {
                return  '<label style="color: #fff;">Drawn Distance: N/A </label></br>';
                //return "Drawn Distance: N/A";
            } else {
                for (var i = 0; i < latlngs.length-1; i++) {
                    distance += latlngs[i].distanceTo(latlngs[i+1]);
                }
                distance = formatunits(distance, 'distance', 1);
                var popuphtml = `<label style="width: 100%; text-align: center;">Drawn Distance</label>
                                <div>`+ distance +`</div>`;
                return popuphtml;
            }
        }
        return null;
    };
  
    map.on(L.Draw.Event.DRAWSTART, function (event) {
      drawmode = 1;
    });
    map.on(L.Draw.Event.EDITSTART, function (event) {
      drawmode = 1;
    });
    map.on(L.Draw.Event.DRAWSTOP, function (event) {
      drawmode = 0;
    });
    map.on(L.Draw.Event.EDITSTOP, function (event) {
      drawmode = 0;
    });
  
  
    // Object created - bind popup to layer, add to feature group
    map.on(L.Draw.Event.CREATED, function(event) {
        var layer = event.layer;
        drawnItems.addLayer(layer);
        var content = getPopupContent(layer);
        console.log(layer._leaflet_id);
        //content = '<div><div class="btn btn-success" onclick="add_user_prop(' + layer._leaflet_id + ')">Protect Property now</div>&nbsp;&nbsp;<div class="btn btn-danger" onclick="clear_drawn_items()">Redraw</div></div><div class="popup-division"></div><div style="text-align:center; background: #337ab7 !important;">' + content + "</div>";
        content = '<div>' + content + "</div>";
        if (content !== null) {
            layer.bindPopup(content);
        }
        layer.openPopup();
    });
  
    // Object(s) edited - update popups
    map.on(L.Draw.Event.EDITED, function(event) {
        var layers = event.layers,
            content = null;
        layers.eachLayer(function(layer) {
            content = getPopupContent(layer);
            if (content !== null) {
                layer.setPopupContent(content);
            }
        });
    });
  }
  
  var location_accuracy_circle, location_marker;
  var maphit = 0;
  function leaflet_addgeolocation(map){
    //-------------------- Geolocation
    map.locate({setView: false,
       //maxZoom: 18,
       timeout:3000,
       watch:true,
       enableHighAccuracy:true
    });
  
    L.control.gpsbutton({ position: 'bottomleft' }).addTo(map);
  
    function onLocationFound(e) {
      //maptonavigation(e.latlng);
      var radius = e.accuracy / 2;
      function within(){
        if (radius > 1000){
          return (radius/1000)+" KM"
        } else {
          return radius+" meters"
        }
      }
      if (map.hasLayer(location_accuracy_circle)){
        map.removeLayer(location_accuracy_circle);
      }
      location_accuracy_circle = L.circle(e.latlng, {
        radius: radius,
        fill: true,
        fillColor: '#5e98ff',
        fillOpacity: 0.4,
        stroke: false,
        interactive: false,
        'background-blend-mode': 'darken'
      }).addTo(map);
      if (map.hasLayer(location_marker)){
         map.removeLayer(location_marker);
      }
      var popuphtml = '<div style="line-height: 25px;    text-align: left;   padding: 10px; background: white; width: max-content;">\
                        <p><b>You are within ' + within() + ' from this point</b></p>\
                      </div>\
                      ';
      location_marker = L.circleMarker(e.latlng, {
        radius: 8,
        fill: true,
        fillColor: '#4487ef',
        fillOpacity: 0.9,
        stroke: true,
        color: '#fff',
        weight: 2,
        opacity: 0.7,
        bubblingMouseEvents: false,
        interactive: true
      }).bindPopup(popuphtml).addTo(map);//.openPopup();
      handleafterlocate(e);
      if ((maphit <= 0) && (map.hasLayer(location_marker))){
        //map.setView(location_marker._latlng, 18);
        //location_marker.openPopup();
        //map.fitBounds(location_accuracy_circle.getBounds());
        maphit++;
      } else {
        map.closePopup();
      }
    }
    map.on('locationfound', onLocationFound);
  }
  
  function zoom_to_gps_location(){
    if (location_marker){
      map.setView(location_marker._latlng, 16);
      //location_marker.openPopup();
      //console.log(location_marker);
    } else {
      alert('Searching for Location');
    }
  }
  
  L.Control.gpsbutton = L.Control.extend({
    onAdd: function (map) {    
      var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control');
      //container.style.backgroundColor = 'white';
      //container.style.width = '30px';
      //container.style.height = '30px';
      container.innerHTML = '<a class="leaflet-control-zoom-in" style="padding: 2px;" href="#" title="GPS" role="button" aria-label="GPS"><i style="color: gray;" class="fas fa-compass"></i></a>'
   
      container.onclick = function(){
        zoom_to_gps_location();
      }
      return container;
    },
    onRemove: function(map) {
        // Nothing to do here
    }   
  });
  L.control.gpsbutton = function(opts) {
    return new L.Control.gpsbutton(opts);
  }
  
  function handleafterlocate(e){
    var popuphtml = location_marker.getPopup().getContent();
    popuphtml = popuphtml + `
    <div class="text-center" style="padding:10px; background: #fff;">
      Your Location
      <pre>` + location_marker._latlng.lat.toFixed(5) + ", " + location_marker._latlng.lng.toFixed(5) + `</pre>
    </div>` ;
    location_marker.bindPopup(popuphtml);
  }
  
  
  
  function clear_drawn_items(){
    var layer_ids = drawnItems._layers;
    for (const [key, layer] of Object.entries(layer_ids)) {
      drawnItems.removeLayer(layer);
    }
  }
  
  function toWKT(layer) {
    var lng, lat, coords = [];
    if (layer instanceof L.Polygon || layer instanceof L.Polyline) {
      var latlngs = layer.getLatLngs();
    for (var i = 0; i < latlngs.length; i++) {
        var latlngs1 = latlngs[i];
        if (latlngs1.length){
        for (var j = 0; j < latlngs1.length; j++) {
          coords.push(latlngs1[j].lng + " " + latlngs1[j].lat);
          if (j === 0) {
            lng = latlngs1[j].lng;
            lat = latlngs1[j].lat;
          }
        }}
        else
        {
          coords.push(latlngs[i].lng + " " + latlngs[i].lat);
          if (i === 0) {
            lng = latlngs[i].lng;
            lat = latlngs[i].lat;
          }}
    };
      if (layer instanceof L.Polygon) {
        return "POLYGON((" + coords.join(",") + "," + lng + " " + lat + "))";
      } else if (layer instanceof L.Polyline) {
        return "LINESTRING(" + coords.join(",") + ")";
      }
    } else if (layer instanceof L.Marker) {
      return "POINT(" + layer.getLatLng().lng + " " + layer.getLatLng().lat + ")";
    }
  };
  //--//--------- Draw Controls End ------------
  
  //----------------- Supporting Functions Start ------------
  function formatunits(value, type, padded=0){
    var units;
    if (type == 'area'){
      var area = value;
         units = {
        kilometers: (area*0.000001).toFixed(2) + " Kilometer²",
        hectare: (area*0.0001).toFixed(2) + " Hectar(s)",
        acre: (area*0.0002471054).toFixed(2) + " Acre(s)",
        bighas: (area*0.00049422).toFixed(2) + " Bighas(s)",
        guntha: (area*0.01).toFixed(2) + " Guntha(s)",
        are: (area*0.01).toFixed(2) + "  Are(s)",
        meters: area.toFixed(2) + " Meter²",
        feet: (area*10.76391).toFixed(2) + " Feet²",
        yards: (area*1.19599).toFixed(2) + " Yards²",
        inches: (area*1550.00310).toFixed(2) + " Inches²",
      }
    } else if (type == 'distance'){
      var distance = value;
      var units = {
        feet: (distance*3.28084).toFixed(2) + " Feet(s)",
        inches: (distance*39.37008).toFixed(2) + " Inch(es)",
        inches: (distance*1.093613).toFixed(2) + " Yard(s)",
        meters: distance.toFixed(2) + " Meter(s)",
        kilometers: (distance*0.001).toFixed(2) + " KMeter(s)",
        miles: (distance*0.0006213712).toFixed(2) + " Mile(s)",
      }
  
    }
    var element = document.createElement("select");
    for (key in units){
      var option = document.createElement("option");
      $(option).text(units[key]);
      if (key == 'meters'){
        $(option).attr("selected", "selected");
      }
      $(element).append(option);
    }
    if (padded > 0){
      element.className = 'form-control';
      var container = document.createElement('div');
      container.className = 'form-group';
      $(container).css({"padding": "10px 10px", "margin-top": "15px", "margin-bottom": "0px"});
      var label = document.createElement('label');
      label.innerHTML = "Select Units: ";
      $(container).append(label);
      $(container).append(element);
      return container.outerHTML;
    } else {
      return element.outerHTML;
    }
  }