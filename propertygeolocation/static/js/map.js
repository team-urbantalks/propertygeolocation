var map, geolocation;
var maphit = 0;
function initMap(){
    /*// lookup for selection objects
    var selection = {};
    // feature property to act as identifier
    var idProp = 'iso_a3';

    var vtLayer = new VectorTileLayer({
        declutter: true,
        source: new VectorTileSource({
        format: new MVT(),
        url: 'https://ahocevar.com/geoserver/gwc/service/tms/1.0.0/' +
            'ne:ne_10m_admin_0_countries@EPSG%3A900913@pbf/{z}/{x}/{-y}.pbf'
        }),
        style: function(feature) {
        var selected = !!selection[feature.get(idProp)];
        return new Style({
            stroke: new Stroke({
            color: selected ? 'rgba(200,20,20,0.8)' : 'gray',
            width: selected ? 2 : 1
            }),
            fill: new Fill({
            color: selected ? 'rgba(200,20,20,0.2)' : 'rgba(20,20,20,0.9)'
            })
        });
        }
    });

    var map = new Map({
        layers: [
        vtLayer
        ],
        target: 'map',
        view: new View({
        center: [0, 0],
        zoom: 2
        })
    });

    var selectElement = document.getElementById('type');

    map.on('click', function(event) {
        var features = map.getFeaturesAtPixel(event.pixel);
        if (!features) {
        selection = {};
        // force redraw of layer style
        vtLayer.setStyle(vtLayer.getStyle());
        return;
        }
        var feature = features[0];
        var fid = feature.get(idProp);

        if (selectElement.value === 'singleselect') {
        selection = {};
        }
        // add selected feature to lookup
        selection[fid] = feature;

        // force redraw of layer style
        vtLayer.setStyle(vtLayer.getStyle());
    });
    */
    var defaultStyle = new ol.style.Style({
        fill: new ol.style.Fill({
            color: [234,231,221,1]
        }),
        stroke: new ol.style.Stroke({
            color: [182,177,162,1],
            width: 1
        })
    });

    var waterStyle = new ol.style.Style({
        fill: new ol.style.Fill({
            color: [28,121,181,1]
        }),
        stroke: new ol.style.Stroke({
            color: [27,107,159,1],
            width: 1
        })
    });

    var streetStyle = new ol.style.Style({
        fill: new ol.style.Fill({
            color: [111,44,173,1]
        }),
        stroke: new ol.style.Stroke({
            color: [93,32,150,1],
            width: 1
        })
    });

    function styleFunction(feature, resolution){
        if (feature.get('type') == 'water' || feature.get('layer') == 'water_areas' || feature.get('layer') == 'water_lines'){
            return [waterStyle];
        }
        if (feature.get('layer') == 'transport_lines'){
            return [streetStyle];
        }
        if (feature.get('layer') == 'country_polygons' || feature.get('layer') == 'landuse_areas'){
            return null; // return null for no style to be applied
        }
        return [defaultStyle];
    }


    var osmlayer = new ol.layer.VectorTile({
        title: 'OSM-Vector',
        type: 'base',
        visible: false,
        declutter: true,
        source: new ol.source.VectorTile({
            format: new ol.format.MVT(),
            url: 'https://osm-lambda.tegola.io/v1/maps/osm/{z}/{x}/{y}.pbf',
        }),        
        style:styleFunction
    });

    var mapboxlayer = new ol.layer.VectorTile({
        title: 'OSM-Mapbox',
        type: 'base',
        visible: false,
        declutter: true,
        source: new ol.source.VectorTile({
            format: new ol.format.MVT(),
            url: 'https://{a-d}.tiles.mapbox.com/v4/mapbox.mapbox-streets-v6/{z}/{x}/{y}.vector.pbf?access_token=pk.eyJ1IjoiYWhvY2V2YXIiLCJhIjoiRk1kMWZaSSJ9.E5BkluenyWQMsBLsuByrmg',
        }),
        style: createMapboxStreetsV6Style(ol.style.Style, ol.style.Fill, ol.style.Stroke, ol.style.Icon, ol.style.Text)
    });

    var osmtilelayer = new ol.layer.Tile({
        title: 'OSM',
        type: 'base',
        visible: false,
        source: new ol.source.OSM()
    });

    var google_maps = new ol.layer.Tile({
        title: 'G-Maps',
        type: 'base',
        visible: false,
        source: new ol.source.OSM({
            url: 'http://mt{0-3}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',
            /*attributions: [
                new ol.Attribution({ html: '© Google' }),
                new ol.Attribution({ html: '<a href="https://developers.google.com/maps/terms">Terms of Use.</a>' })
            ]*/
        }),
    })

    var google_imagery = new ol.layer.Tile({
        title: 'G-Satellite',
        type: 'base',
        visible: true,
        source: new ol.source.OSM({
            url: 'http://mt{0-3}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',
            /*attributions: [
                new ol.Attribution({ html: '© Google' }),
                new ol.Attribution({ html: '<a href="https://developers.google.com/maps/terms">Terms of Use.</a>' })
            ]*/
        }),
    })

    geolocated_property_layer = new ol.layer.Tile({
        title: 'Geolocated Properties',
        //type: 'base',
        visible: false,
        source: new ol.source.TileWMS({
            url: avenue_wms_api,
            params: {'LAYERS': 'avenue_geolocation:geolocated_property', 'TILED': true},
            serverType: 'geoserver',
            // Countries have transparency, so do not fade tiles:
            transition: 0
        }),
        zIndex: 1900
    })

    var district_layer = new ol.layer.Tile({
        title: 'District',
        //type: 'base',
        visible: true,
        source: new ol.source.TileWMS({
            url: avenue_wms_api,
            params: {'LAYERS': 'avenue:gis_admin_district', 'TILED': true},
            serverType: 'geoserver',
            // Countries have transparency, so do not fade tiles:
            transition: 0
        }),
        zIndex: 1800
    })

    var subdistrict_layer = new ol.layer.Tile({
        title: 'Mandal',
        //type: 'base',
        visible: true,
        source: new ol.source.TileWMS({
            url: avenue_wms_api,
            params: {'LAYERS': 'avenue:gis_admin_subdistrict', 'TILED': true},
            serverType: 'geoserver',
            // Countries have transparency, so do not fade tiles:
            transition: 0
        }),
        zIndex: 1798
    })

    var village_layer = new ol.layer.Tile({
        title: 'Village',
        //type: 'base',
        visible: true,
        source: new ol.source.TileWMS({
            url: avenue_wms_api,
            params: {'LAYERS': 'avenue:gis_admin_village', 'TILED': true},
            serverType: 'geoserver',
            // Countries have transparency, so do not fade tiles:
            transition: 0
        }),
        zIndex: 1790,
        //extent: [952014.59,5571269.68,1272301.10,5862273.22]
        maxResolution: 10
    })

    parcel_layer = new ol.layer.Tile({
        title: 'Parcel',
        //type: 'base',
        visible: true,
        source: new ol.source.TileWMS({
            url: avenue_wms_api,
            params: {'LAYERS': 'avenue:gis_ap_parcel', 'TILED': true},
            serverType: 'geoserver',
            // Countries have transparency, so do not fade tiles:
            transition: 0
        }),
        zIndex: 1699,
        opacity: 0.5,
        //minZoom: 8,
        //maxZoom: 15
        //minResolution: 50000,
        maxResolution: 7
    })

    amaravati_roads_extended_layer = new ol.layer.Tile({
        title: 'Road Layer',
        //type: 'base',
        visible: true,
        source: new ol.source.TileWMS({
            url: avenue_wms_api,
            params: {'LAYERS': 'avenue:gis_amaravati_roads_extended', 'TILED': true},
            serverType: 'geoserver',
            // Countries have transparency, so do not fade tiles:
            transition: 0
        }),
        zIndex: 1699,
        opacity: 0.5,
        //minZoom: 8,
        //maxZoom: 15
        //minResolution: 50000,
        maxResolution: 100
    })

    amaravati_roads_labels_layer = new ol.layer.Tile({
        title: 'Road Layer',
        //type: 'base',
        visible: true,
        source: new ol.source.TileWMS({
            url: avenue_wms_api,
            params: {'LAYERS': 'avenue:gis_amravati_roadline', 'TILED': true},
            serverType: 'geoserver',
            // Countries have transparency, so do not fade tiles:
            transition: 0
        }),
        zIndex: 4900,
        opacity: 0.7,
        //minZoom: 8,
        //maxZoom: 15
        //minResolution: 50000,
        maxResolution: 100
    })

    // Drawing Layer
    
    var vectorsource = new ol.source.Vector({wrapX: false});

    drawnvector_layer = new ol.layer.Vector({
        source: vectorsource,
        'title': 'Editable Drawing Layer',
        style: new ol.style.Style({
          fill: new ol.style.Fill({
            color: 'rgba(255, 255, 0, 0.2)'
          }),
          stroke: new ol.style.Stroke({
            color: '#0072ff',
            width: 2
          }),
          image: new ol.style.Circle({
            radius: 7,
            fill: new ol.style.Fill({
              color: '#fff200'
            })
          })
        }),
        zIndex: 10000,
    });

    // For Layer Control    
    var layers = [
        new ol.layer.Group({
            'title': 'Boundary',
            layers: [
                amaravati_roads_labels_layer,
                district_layer,
                subdistrict_layer,
                village_layer,
                parcel_layer,
                amaravati_roads_extended_layer,
            ].reverse()
        }),
        new ol.layer.Group({
            'title': 'Base maps',
            layers: [
                osmtilelayer,
                osmlayer,
                mapboxlayer,
                google_imagery
            ]
        }),
        drawnvector_layer,
        //geolocated_property_layer,
    ]
    
    var mapview = new ol.View({
        center: ol.proj.transform([80.5, 16.5], 'EPSG:4326', 'EPSG:3857'),
        zoom: 12
    })

    map = new ol.Map({
        target: 'MapCanvas',
        layers: layers,
        view: mapview,        
    });
    
    // Refresh cached layers
    RefreshSavedProperty();

    var layerSwitcher = new ol.control.LayerSwitcher({
        tipLabel: 'Légende' // Optional label for button
    });

    map.addControl(layerSwitcher);

    var mousePositionControl = new ol.control.MousePosition({
        coordinateFormat: ol.coordinate.createStringXY(4),
        projection: 'EPSG:4326',
        // comment the following two lines to have the mouse position
        // be placed within the map.
        className: 'custom-mouse-position',
        target: document.getElementById('mouse-position'),
        undefinedHTML: '&nbsp;'
    });

    map.addControl(mousePositionControl);

    map.addControl(new ol.control.FullScreen());
    map.addControl(new ol.control.ScaleLine());

    map.on("moveend", function(e){
        console.log('Zoom:'+ map.getView().getZoom() + " Resolution: " +  map.getView().getResolution());
    });

    // Add draw controls
    var draw, snap; // global so we can remove it later    
    var typeSelect = document.getElementById('type');

    function addInteractions() {
        draw = new ol.interaction.Draw({
            source: vectorsource,
            type: typeSelect.value,
            style: new ol.style.Style({
                image: 
                //Start of the Icon style
                /*new ol.style.Icon({
                    src: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Crosshairs_Red.svg/1024px-Crosshairs_Red.svg.png',
                    size: [10, 10],
                    opacity: 1
                })*/
                //Start of the circle style
                /*new ol.style.Circle({
                    fill: new ol.style.Fill({
                        color: 'green'
                    }),
                    stroke: new ol.style.Stroke({
                        width: 3,
                        color: 'red'
                    }),
                    radius: 8
                })*/
                //Start of the star style
                new ol.style.RegularShape({
                    fill: new ol.style.Fill({
                        color: 'red'
                    }),
                    points: 4,
                    radius1: 15,
                    radius2: 1
                }),
                stroke: new ol.style.Stroke({
                    color: 'blue',
                    width: 5
                }),
                fill: new ol.style.Fill({
                    color: 'green'
                })
            })
        });
        map.addInteraction(draw);
        snap = new ol.interaction.Snap({source: vectorsource});
        map.addInteraction(snap);
    }

    var modify = new ol.interaction.Modify({source: vectorsource});
    map.addInteraction(modify);

    /**
     * Handle change event.
     */
    typeSelect.onchange = function() {
        map.removeInteraction(draw);
        map.removeInteraction(snap);
        addInteractions();
    };

    addInteractions();

    /* Add Geolocation Controls Start */

    geolocation = new ol.Geolocation({
        // enableHighAccuracy must be set to true to have the heading value.
        tracking: true,
        trackingOptions: {
            enableHighAccuracy: true
        },
        projection: mapview.getProjection()
    });

    function el(id) {
        return document.getElementById(id);
    }

      // update the HTML page when the position changes.
      /*geolocation.on('change', function() {
        el('accuracy').innerText = geolocation.getAccuracy() + ' [m]';
        el('altitude').innerText = geolocation.getAltitude() + ' [m]';
        el('altitudeAccuracy').innerText = geolocation.getAltitudeAccuracy() + ' [m]';
        el('heading').innerText = geolocation.getHeading() + ' [rad]';
        el('speed').innerText = geolocation.getSpeed() + ' [m/s]';
      });*/

      // handle geolocation error.
    geolocation.on('error', function(error) {
        var info = document.getElementById('info');
        info.innerHTML = error.message;
        info.style.display = '';
    });

    var accuracyFeature = new ol.Feature();
        geolocation.on('change:accuracyGeometry', function() {
        accuracyFeature.setGeometry(geolocation.getAccuracyGeometry());
    });

    var positionFeature = new ol.Feature();
    positionFeature.setStyle(new ol.style.Style({
        image: new ol.style.Circle({
            radius: 6,
            fill: new ol.style.Fill({
                color: '#3399CC'
            }),
            stroke: new ol.style.Stroke({
                color: '#fff',
                width: 2
            })
        })
    }));

    geolocation.on('change:position', function() {
        var coordinates = geolocation.getPosition();
        positionFeature.setGeometry(coordinates ? new ol.geom.Point(coordinates) : null);
        geolocationFound(coordinates);
    });

    lochit = 0;
    function geolocationFound(coordinates){
        if (lochit < 1){
            map.getView().setCenter(coordinates);
        }
        if ( document.getElementById('track-gps-location').checked == true ){
            map.getView().setCenter(coordinates);
        }
        lochit ++ ;
    }

    var gps_layer = new ol.layer.Vector({
        map: map,
        source: new ol.source.Vector({
            //features: [accuracyFeature, positionFeature]
            features: [positionFeature]
        })
    });

    var element = document.createElement('div');
    element.id = 'zoomToGps';
    element.className = 'custom-control ol-unselectable ol-control ol-full-screen';
    ol.control.Control.call(this, {
        element: element
    });
    ol.inherits(element, ol.control.Control);

    zoomToGpsButton = function(opt_options) {
        var element = document.createElement('div');
        element.id = 'zoomToGps';
        element.className = 'ol-unselectable ol-control';
        element.innerHTML = '<button type="button" title="Toggle full-screen" onclick="zoom_to_gps_location()">G</button>';
        ol.control.Control.call(this, {
            element: element,
            //target: zoom_to_gps_location
        });
    };
    ol.inherits(zoomToGpsButton, ol.control.Control);

    map.addControl(new zoomToGpsButton);

    /* Add Geolocation Controls End */
}

// Validate Points and make Convex Hull out of it
var validatedlayer;

function validate_geometry(){
    //console.log(drawnvector_layer);
    try{
        validatedlayer.getSource().clear();
    } catch { 
        
        }
    
    var features = drawnvector_layer.getSource().getFeatures();
    console.log(features.length);
    if (features.length > 2){        
        var geojson_tool = new ol.format.GeoJSON();
        var geojson = geojson_tool.writeFeatures(features, { 
            featureProjection: 'EPSG:3857',
            dataProjection: 'EPSG:4326',
        });
        geojson = JSON.parse(geojson);
        console.log(geojson);

        
        //var options = {units: 'kilometers', maxEdge: 0.3};
        //var boundary = turf.concave(geojson, options);

        var boundary = turf.convex(geojson, {concavity: 1});
        console.log(boundary);

        var vectorSource = new ol.source.Vector({
            features: geojson_tool.readFeatures(boundary, {
                featureProjection: 'EPSG:3857'
            })
        });

        var style = drawnvector_layer.getStyle();

        validatedlayer = new ol.layer.Vector({
            source: vectorSource,
            style: style,
            zIndex: drawnvector_layer.getZIndex-1
        });

        map.addLayer(validatedlayer);
    } else {
        alert('Please mark Atleast 3 Points to Proceed')
    }
}

// Clear Last Drawn Geometry
function clear_last_Feature(){
    var vectorSource = drawnvector_layer.getSource();
    var features = vectorSource.getFeatures();
    if (features.length){
        var lastFeature = features[features.length - 1];
        vectorSource.removeFeature(lastFeature);
    }
}

// Clear All Drawn Geometry
function clear_all(){
    if (validatedlayer){
        validatedlayer.getSource().clear();
    }
    if (drawnvector_layer){
        drawnvector_layer.getSource().clear();
    }
}

// Sync Geometry
function RefreshSavedProperty(){
    geolocated_property_layer.getSource().updateParams({"time": Date.now()});
}

function saveproperty(){
    validate_geometry();
    var fieldremarks = document.getElementById('fieldremarks').value;

    if (validatedlayer){
        var features = validatedlayer.getSource().getFeatures();
        if (features.length > 0){        
            var geojson_tool = new ol.format.GeoJSON();
            var geojson = geojson_tool.writeFeatures(features, { 
                featureProjection: 'EPSG:3857',
                dataProjection: 'EPSG:4326',
            });
            geojson = JSON.parse(geojson);

            function _callback(data){
                data = JSON.parse(data);
                console.log(data['propid'])
                //RefreshSavedProperty();
                //clear_all();
                //document.getElementById('fieldremarks').value = "";
                if (data.length && ('propid' in data[0]) ){
                    window.location = "./?propid="+ data[0]['propid'];
                } else {
                    alert('Failed to save Property');
                }
            }

            params = {
                'url' : add_property_api,
                'data': {
                    'property' : geojson,
                    'fieldremarks' : fieldremarks
                },
                'callback' : _callback
            }
            hitPostApi(params);
        } else {
            alert('No Property to Push')
        }
    }
    }
    

function zoom_to_gps_location() {
    var loc = geolocation.getPosition();
    if (loc){
        map.getView().setCenter(geolocation.getPosition());
    }
}

function handleUrlParams(){
    var url = window.location.href;
    var urlParams = getAllUrlParams(url);
    lochit ++;

    var propid = urlParams["propid"];
    if (typeof propid === "string"){
        var propids = propid.split(',');
        getProperty(propids);
    }

    if (urlParams["latlng"]){
        let latlng = decodeURIComponent(urlParams["latlng"]).trim();
        console.log(latlng);
        go_to_latlng(latlng);
    }
}

function getProperty(propids){
    if (propids.length){
        params = {
            propid : propids.toString()
        }
        fetch_data(get_property_api, params)
        .then(
            data => loadPropertyOnMap(data)
        )
    }
}

var getText = function(feature) {
    var text = feature.get('propid').toString();
    //console.log(text);
    return text;
};

var createTextStyle = function(feature) {
  return new ol.style.Text({
    //textAlign: 'center',
    //textBaseline: 'middle',
    font: '16px Arial',
    text: getText(feature),
    fill: new ol.style.Fill({color: 'black'}),
    stroke: new ol.style.Stroke({color: 'white', width: 5})
  });
};

var getStyleFunction = function(feature){
    var style = new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: '#cc0c0c',
            width: 3,
            lineDash: [4, 8]
        }),
        fill: new ol.style.Fill({
            color: 'rgba(255, 255, 0, 0.2)'
        }),
        text: createTextStyle(feature)
    })

    return style;
}

function loadPropertyOnMap(data){
    if ('Error' in data){
        var alert_text = "Error: " + data["Error"]
        alert(alert_text);
    }
    if ("property_geojson" in data){
        var vectorSource = new ol.source.Vector({
            features: new ol.format.GeoJSON().readFeatures(data["property_geojson"],{
                featureProjection: 'EPSG:3857'
            })
        });

        var vectorLayer = new ol.layer.Vector({
            source: vectorSource,
            style: getStyleFunction,
            zIndex: 2000
        });

        map.addLayer(vectorLayer);

        var extent = vectorLayer.getSource().getExtent();
        map.getView().fit(extent, size = map.getSize());

    }
}

/* on Window load Triggers */
$( document ).ready(function() {
    initMap();
    handleUrlParams();
    maphit++;
});