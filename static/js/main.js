/* Configrables */
const api_server = 'https://api.avenue.in';
const geoserverwmsapi = api_server + '/geoserver/wms';
const avenue_wms_api = geoserverwmsapi;
const nodejsapi = api_server + '/api/nodejsapi';
const autocompleteSearchApi = nodejsapi + '/autocomplete-test/';
const listApi = nodejsapi + '/listapi/';

add_property_api = '/api/v1/add-property/'
get_property_api = '/api/v1/get-property/'
delete_property_api = '/api/v1/delete-property/'




/// Common Functions

function hitPostApi(params) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      console.log(this.responseText);
      params.callback(this.responseText);
    }
  };
  xhttp.open("POST", params.url);
  xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
  xhttp.setRequestHeader('X-CSRFToken', csrf_token)
  xhttp.send(JSON.stringify(params.data));
}

function buildUrl(url, parameters) {
  let qs = "";
  for (const key in parameters) {
      if (parameters.hasOwnProperty(key)) {
          const value = parameters[key];
          qs +=
              encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
      }
  }
  if (qs.length > 0) {
      qs = qs.substring(0, qs.length - 1); //chop off last "&"
      url = url + "?" + qs;
  }

  return url;
}


var absolutePath = function(href) {
  var link = document.createElement("a");
  link.href = href;
  return link.href;
}


async function fetch_data(url, params={}){
  if (params){
    url = buildUrl(url, params)
  }
  let response = await fetch(url, 
    {
      credentials: 'same-origin',
    }  
  );

  let data = await response.json();

  return data;  
}

function shareThisPage(){
  let url = document.location.href;
  copyStringToClipboard(url.toString()); 
  const canonicalElement = document.querySelector('link[rel=canonical]');
  if (canonicalElement !== null) {
    url = canonicalElement.href;
  }
  navigator.share({url: url});
}

function shareThis(x){
  copyStringToClipboard(x.toString());
  navigator.share({url: x});
}

if(!(navigator.share)){
  navigator.share = function (){
    alert('copied to clipboard');
  }
}

function copyStringToClipboard(str) {
  // Create new element
  var el = document.createElement('textarea');
  // Set value (string to be copied)
  el.value = str;
  // Set non-editable to avoid focus and move outside of view
  el.setAttribute('readonly', '');
  el.style = {position: 'absolute', left: '-9999px'};
  document.body.appendChild(el);
  // Select text inside element
  el.select();
  // Copy text to clipboard
  document.execCommand('copy');
  // Remove temporary element
  document.body.removeChild(el);
}

function getAllUrlParams(url) {

  // get query string from url (optional) or window
  var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

  // we'll store the parameters here
  var obj = {};

  // if query string exists
  if (queryString) {

    // stuff after # is not part of query string, so get rid of it
    queryString = queryString.split('#')[0];

    // split our query string into its component parts
    var arr = queryString.split('&');

    for (var i = 0; i < arr.length; i++) {
      // separate the keys and the values
      var a = arr[i].split('=');

      // set parameter name and value (use 'true' if empty)
      var paramName = a[0];
      var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];

      // (optional) keep case consistent
      paramName = paramName.toLowerCase();
      if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();

      // if the paramName ends with square brackets, e.g. colors[] or colors[2]
      if (paramName.match(/\[(\d+)?\]$/)) {

        // create key if it doesn't exist
        var key = paramName.replace(/\[(\d+)?\]/, '');
        if (!obj[key]) obj[key] = [];

        // if it's an indexed array e.g. colors[2]
        if (paramName.match(/\[\d+\]$/)) {
          // get the index value and add the entry at the appropriate position
          var index = /\[(\d+)\]/.exec(paramName)[1];
          obj[key][index] = paramValue;
        } else {
          // otherwise add the value to the end of the array
          obj[key].push(paramValue);
        }
      } else {
        // we're dealing with a string
        if (!obj[paramName]) {
          // if it doesn't exist, create property
          obj[paramName] = paramValue;
        } else if (obj[paramName] && typeof obj[paramName] === 'string'){
          // if property does exist and it's a string, convert it to an array
          obj[paramName] = [obj[paramName]];
          obj[paramName].push(paramValue);
        } else {
          // otherwise add the property
          obj[paramName].push(paramValue);
        }
      }
    }
  }

  return obj;
}



function checknull(x){
  if(!(x)){
    x = '';
  }
  return x;
}

function copytoclipboard(x) {
  var dummyContent = x;
  var dummy = $('<input>').val(dummyContent).appendTo('body').select();
  document.execCommand('copy');
}
/*function copytoclipboard(text){
  window.clipboardData.setData("Text", text);
}*/
function checkenterpressed(_event, callback){
  if (_event.keyCode == 13){
    callback();
  }
}

function titleCase(str) {
  str = str.toLowerCase().split(' ');
  for (var i = 0; i < str.length; i++) {
  str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
  }
  return str.join(' ');
}

function fittobounds(data){
  var extent = {};
  if (data.extent){
    extent = data.extent;
    //console.log("Extent", extent)
  } else if (data.bbox){
    try {
      var bbox = JSON.parse(data.bbox);
      //console.log(bbox);
      var north_east_coordinates = bbox.coordinates[0][2];
      var south_west_coordinates = bbox.coordinates[0][0];
      extent.xmax = north_east_coordinates[0];
      extent.xmin = south_west_coordinates[0];
      extent.ymax = north_east_coordinates[1];
      extent.ymin = south_west_coordinates[1];
    } catch {
      var bbox = data.bbox.slice(4).slice(0, -1);
      bbox = bbox.replace(/,/g, " ").split(" ");
      extent.xmax = bbox[2];
      extent.xmin = bbox[0];
      extent.ymax = bbox[3];
      extent.ymin = bbox[1];
    }
  }
  if (extent.ymin){
    if (extent.ymin == 'NaN'){
      return 0;
    }
    map.fitBounds([
        [extent.ymin, extent.xmin],
        [extent.ymax, extent.xmax]
    ]);
  }
}

if(typeof(String.prototype.trim) === "undefined") {
    String.prototype.trim = function()
    {
        return String(this).replace(/^\s+|\s+$/g, '');
    };
}
