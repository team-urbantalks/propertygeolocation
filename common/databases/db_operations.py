from .db_config import db_configs

import psycopg2
from psycopg2 import pool, extras
from multiprocessing.pool import ThreadPool

# init DB POOLS
db_pools = {}

# Postgres Connection Pool
def init_db(db_config): 
	try:
		db_pool = psycopg2.pool.ThreadedConnectionPool(5, 50, host=db_config['host'], database=db_config['database'], user=db_config['user'], password=db_config['password'], cursor_factory=psycopg2.extras.RealDictCursor)#, connect_timeout=3)
		print("PostgreSQL Connection pool created with Database: ", db_config['database'], " at host ", db_config['host'], "successfully")
		return db_pool
	except (Exception, psycopg2.DatabaseError) as error :
	    print ("Error while connecting to PostgreSQL database: ", db_config, " | Error:", error, "   |   ", psycopg2.DatabaseError)

for x in db_configs:
	db_pools[x] = init_db(db_configs[x])

def get_tested_pool(db_pool_name): 
	try:
		db_pool = db_pools[db_pool_name]
		conn = db_pool.getconn()
		cur = conn.cursor()
		query = "select 1 as test"
		cur.execute(query)
		data = cur.fetchone()
		cur.close()
		try:
			db_pool.putconn(conn)
		except Exception as e:
			print(e)
		return db_pool
	except:
		print("Reconnecting to the pool", db_pool_name)
		db_pool = init_db(db_configs[db_pool_name])
		db_pools[db_pool_name] = db_pool
		return db_pool
	
def get_features():
	pass
	
def querydb(query, db_pool_name):
	try:
		db_pool = get_tested_pool(db_pool_name)
		conn = db_pool.getconn()
		cur = conn.cursor()
		cur.execute(query)
		data = cur.fetchall()
		cur.close()		
		try:
			db_pool.putconn(conn)
		except Exception as e:
			print(e)
		return data
	except Exception as e:
		rollback_db(db_pool_name)
		raise Exception(e)

def execdb(query, db_pool_name, key=None):
	try:
		db_pool = get_tested_pool(db_pool_name)
		conn = db_pool.getconn(key)
		cur = conn.cursor()
		cur.execute(query)
		conn.commit()	
		data = cur.fetchall()
		cur.close()
		try:
			db_pool.putconn(conn)
		except Exception as e:
			print(e)
		return data    
	except Exception as e:
		rollback_db(db_pool_name)
		raise Exception(e)

 # Use rollback_db if you get any transaction related error
def rollback_db(db_pool_name):
	db_pool = get_tested_pool(db_pool_name)
	conn = db_pool.getconn()
	cur = conn.cursor()
	cur.execute("ROLLBACK")
	conn.commit()
	cur.close()    
	try:
		db_pool.putconn(conn)
	except Exception as e:
		print(e)
    
def showalltables(db_pool_name):
	query = 'SELECT * FROM pg_catalog.pg_tables'
	db_pool = get_tested_pool(db_pool_name)
	data = querydb(query, db_pool)
	tables = []
	for i in data:
		if i['schemaname'] == 'public':
			tables.append(i['tablename'])
			#print(i['tablename'])
	return tables


threadpool_query = ThreadPool(100)
def querydb_multi(querylist):
	#data_list = []
	data_list_ = {}
	results = threadpool_query.imap(parallel_query_helper, querylist)
	for i, j in enumerate(results):
		#data_list.append(j)
		data_list_[querylist[i][1]] = j
	#threadpool_query.close()
	#threadpool_query.terminate()
	#return data_list
	return data_list_

def parallel_query_helper(query):
	if (len(query) < 2):
		print('invalid', query)
	else:
		try:
			query[2]['db_pool']
		except:
			print('error', query)
	db_pool_name = query[2]['db_pool']
	return querydb(query[0], db_pool_name)